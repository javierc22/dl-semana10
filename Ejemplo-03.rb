# --------- USO DEL METODO DE CLASE ------- #
class Product
  attr_reader :name, :blue, :red, :green
  def initialize(name, blue, red, green)
    @name = name
    @blue = blue.to_i
    @red = red.to_i
    @green = green.to_i
  end

  def self.load_data(filename)
    data = File.open(filename, 'r') { |file| file.readlines.map(&:chomp) }
    
    products = []
    data.each do |line|
      ls = line.split(', ')
      products << Product.new(*ls)
    end

    return products
  end
end

products = Product.load_data('Ejemplo-03.txt')
products.each { |e| puts "#{e.name}"} # => Producto1 Producto2 Producto3