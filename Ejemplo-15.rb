# ------------- MIXIN --------------- #
# Los módulos se pueden incluir o extender por otras clases:
module Foo
  def bar
    puts 'Hola'
  end
end

# 'include' permite integrar los métodos de un módulo como métodos de instancia
class Example
  include Foo
end
# 'extend' permite integrar los métodos de un módulo como métodos de clase
class Example2
  extend Foo
end

a = Example.new
a.bar # => Hola

b = Example2.bar
b # => Hola
