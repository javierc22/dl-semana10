require_relative 'Ejemplo-11A'

class Perro < Mamifero
  def hablar
    'guau'
  end
end

puts Perro.new.hablar # => guau
puts Mamifero.new.vertebrado # => true
puts Perro.new.vertebrado # => true

# Clases relacionadas con una clase (ancestros):
print Perro.ancestors # => [Perro, Mamifero, Object, Kernel, BasicObject]
puts

# Preguntar si la instancia (objeto) es de una clase en específico:
puts Perro.new.is_a? Perro # => true
puts Perro.new.is_a? Mamifero # => true
puts Perro.new.is_a? Numeric # => false (porque 'Perro' no es un número)