# ------------ MÓDULOS Y HERENCIA ----------- #
# Sólo puedes heredar de una clase, pero puedes hacer mixing de muchos módulos.

# No se puede instanciar módulos, los módulos son para hacer namespacing y para agrupar métodos en común.

module Nadador
  def nadar
    puts "Puedo nadar!"
  end
end

class Animal; end

class Pez < Animal
  include Nadador #mixing en módulo Nadador
end

class Mamifero < Animal
end

class Gato < Mamifero
end

class Perro < Mamifero
  include Nadador #mixing en módulo Nadador
end

cocky = Perro.new
nemo = Pez.new
garfield = Gato.new
cocky.nadar # => Puedo nadar!
nemo.nadar # => Puedo nadar!
galfield.nadar # => NoMethodError: undefined method