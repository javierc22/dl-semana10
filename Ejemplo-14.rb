# ---------- CLASES DENTRO DE MÓDULOS ------------ #
module Mammal
  class Dog
    def speak(sound)
      p "#{sound}"
    end
  end
  
  class Cat
    def say_name(name)
      p "#{name}"
    end
  end
end

# Llamamos a las clases dentro de los módulos anexando el nombre de la clase con el
# nombre del módulo por medio de : : 

# Instanciando clases específicas dentro de módulos:
buddy = Mammal::Dog.new
kitty = Mammal::Cat.new
buddy.speak('Arf!') # => "Arf!"
kitty.say_name('kitty') # => "kitty" 