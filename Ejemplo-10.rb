# ---------- SUPER ----------- #
# Super nos permite llamar a un método de la clase Padre que se llame exactamente igual

class Parent
  def foo
    puts 'hola'
  end
end

class Child < Parent
  def foo
    puts 'antes'
    super # puts 'hola
    puts 'después'
  end
end

puts Child.new.foo # => antes hola después