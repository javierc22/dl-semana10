#config.ru
require 'rack'
class MiPrimeraWebApp
  def call(env)
    if env['REQUEST_PATH'] == '/hola'
      [200, {'Content-Type' => 'text/html'}, ["<h1> Pagina 1 </h1>"]]
    else
      [200, {'Content-Type' => 'text/html'}, [File.read('index.html')]]
    end
  end
end

run MiPrimeraWebApp.new