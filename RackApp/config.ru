# ---------- MI PRIMERA PP CON RACK -------- #
# Rack es una herramienta para construir aplicaicones web en Ruby.
# Es utilizado como base para Rails, Padrino y Sinatra.

# Ejecutar con: $ rackup
# en localhost:9292

#config.ru
require 'rack'

class MiPrimeraWebApp # Clase con el método 'call'
  def call(env) # información del request
    @env = env['REQUEST_PATH'] # obtener las URLs
    [200, {'Content-Type' => 'text/html'}, ['<h1> Hola </h1>']] # mostrar 'Hola'
    [200, {'Content-Type' => 'text/html'}, [@env]] # mostrar url's
  end
end

run MiPrimeraWebApp.new