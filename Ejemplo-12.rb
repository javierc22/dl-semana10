# ------- INTRODUCCION A MIXINS --------- #
# Los módulos en ruby tienen diversas funciones, una importante es agrupar objetos, también
# pueden agrupar otros módulos.

# ------------------------------------------------------------------------------
# HACER UN MÓDULO NO ES MUY DISTINTO A HACER UN OBJETO:
# En este caso, self es importante porque nos permite llamar el método desde fuera del módulo.
module Foo
  def self.bar
    puts "Desafío !!!"
    puts "LATAM !!!"
  end
end

Foo.bar # => Desafio !!! LATAM !!!

# ------------------------------------------------------------------------------
# Los módulos pueden tener constantes que son fácilmente accesibles:
module Foo2
  D = 20
end
 
puts Foo2::D # => 20

# ------------------------------------------------------------------------------
# Dentro de los módulos podemos poner objetos:
# De esta forma podemos generar namespaces.
module Foo3
  class Bar
    def initialize()
      puts "hola"
    end
  end
end

Foo3::Bar.new # => hola