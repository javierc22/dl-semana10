# -------------- MÓDULOS VS CLASES --------------- #

# Clases:
class Formula
  @@pi = 3.1415

  def self.pi
    @@pi
  end

  def self.diameter(r)
    2*r
  end

  def self.perimeter(distance)
    diameter(distance) * pi
  end
end

puts Formula.pi # => 3.1415

# Módulo:
module Formulaa
  PI = 3.1415
  def self.diameter(r)
    2*r
  end

  def self.perimeter(distance)
    diameter(distance) * PI
  end
end

puts Formulaa::PI # => 3.1415

# MÓDULOS
# • Sirven para agrupar el código bajo un namespace.
# • Son útiles para guardar constantes.
# • Sirven para implementar mixins.