# --------- SELF ------- #

# Dentro de la clase sirven para crear un método de clase
# Dentro de la instancia sirven para evitar confundir una variable local con un método

class Product
  attr_reader :name, :blue, :red, :green
  def initialize(name, blue, red, green)
    @name = name
    @blue = blue.to_i
    @red = red.to_i
    @green = green.to_i
  end

  # Self dentro de un método de instancia, self es la instancia
  def stock_total
    puts self
  end

  def self.load_data(filename)
    data = File.open(filename, 'r') { |file| file.readlines.map(&:chomp) }
    
    products = []
    data.each do |line|
      ls = line.split(', ')
      products << Product.new(*ls)
    end

    return products
  end
end

products = Product.load_data('Ejemplo-03.txt')
print products[0].stock_total # => #<Product:0x0000000005363558>