# ------------ REFLEXION SOBRE SELF --------------- #
class Perro
  # Self dentro de un método de clase es la 'clase'
  def self.reflexionar
    self
  end
  # Self dentro de un método de instancia es la 'instancia'
  def reflexionar
    self
  end
end

puts Perro.reflexionar # => Perro
puts Perro.new.reflexionar # => #<Perro:0x000000000515acc0>