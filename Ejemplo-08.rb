# ----------- HERENCIA ------------ #
# La herencia es un mecanismo que le permite a una clase
# adoptar los atributos y comportamientos de otra clase.

class Padre # Clase Padre
  attr_accessor :name, :age
  def initialize(name)
    @name = name
    @age = 0
  end
  
  def get_older
    @age += 1
  end
end

class Hijo < Padre # Clase Hija

end

c = Hijo.new("DesafioLatam")
c.get_older
c.get_older
puts c.age # => 2

# ¿POR QUÉ ES IMPORTANTE LA HERENCIA?
# Nos permite reutilizar código y lo tenemos que manejar por que muchas clases de Rails la ocupan.