## Desafío Latam - Semana 10

#### Programación orientada a objetos:

1. Introducción
2. Suma de complejos y mutabilidad
3. Métodos de clase
4. Uso del método de clase
5. Self
6. Reflexión sobre Self
7. Self vs Atributos
8. Variables de clase
9. Repaso de sintaxis
10. Herencia
11. Super
12. Ejercicio Integrador
13. QUIZ

#### Módulos y Mixins:

14. Introducción a Mixins
15. Módulos vs Clases
16. Clases dentro de módulos
17. Mixin
18. Módulos y herencia

#### Rack:

18. Introducción a Rack
19. Mi primera APP con Rack
20. Info del request
21. Response
22. Múltiples páginas
23. Cargando archivos desde fuera
24. QUIZ
