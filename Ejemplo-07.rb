# --------- VARIABLES DE CLASE ------------- #
# Así como las instancias tienen sus métodos y variables,
# Las clases también tienen sus propios métodos (métodos de clase) y variables.
# Las variables de clase en Ruby se crean con En ruby @@

class T
  @@foo = 5

  def self.bar
    @@foo
  end
end

puts T.bar # => 5

# UN EJEMPLO ÚTIL DE VARIABLES DE CLASES: Contador de instancias
class D
  @@instances = 0

  def initialize()
    @@instances +=1
  end
 
  def self.get_number_of_instances
    @@instances
  end
end

10.times do |i|
  D.new
end

puts D.get_number_of_instances # => 10