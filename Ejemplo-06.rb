# --------- SELF VS ATRIBUTOS ---------- #
# Dentro de la instancia sirven para evitar confundir una variable local con un método
class Circle
  attr_accessor :radius
  def initialize
    @radius = 1
  end

  def bigger
    @radius = self.radius + 1
  end

  def to_s
    "círculo de radio #{@radius}"
  end
 end

 c = Circle.new
 c.bigger
 print c