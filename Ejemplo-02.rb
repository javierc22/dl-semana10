# ---------- METODOS DE CLASE ---------- #
# en una CLASE: Se les llama métodos de clase. 
# en un OBJETO: Se les llama métodos de instancia.

class Fabrica
  def self.metodo_de_clase
    puts 'Soy un método de clase!'
  end

  def metodo_de_instancia
    puts 'Soy un método de instancia!'
  end
end

Fabrica.metodo_de_clase # => 'Soy un método de clase!'
Fabrica.new.metodo_de_instancia # => 'Soy un método de instancia!'