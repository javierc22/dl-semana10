# ---------- SUMA DE COMPLEJOS Y MUTABILIDAD ------------ #
class NumeroComplejo
  attr_reader :a, :b
  def initialize(a, b)
    @a = a
    @b = b
  end

  # Suma z1 + z2 en un nuevo objeto
  def +(z2)
    NumeroComplejo.new(@a + z2.a, @b + z2.b)
  end

  # Retorna el resultado en formato to_s
  def to_s
    "#{@a} + #{@b}i"
  end
end

z1 = NumeroComplejo.new(2,3)
z2 = NumeroComplejo.new(4,3)
puts z1 + z2 # => 6 + 6i